# frozen_string_literal: true

require 'spec_helper'

describe '#Car Class' do
  context 'a @car' do
    before :each do
      @id = Faker::Number.digit
      @price_day = Faker::Number.digit
      @price_km = Faker::Number.digit
      @car = Car.new(@id, @price_day, @price_km)
    end

    it 'is an instance of Car' do
      expect(@car).to be_an_instance_of(Car)
    end

    it 'has an id' do
      expect(@car.id).to eq @id
    end

    it 'has a price by_day' do
      expect(@car.price_per_day).to eq @price_day
    end

    it 'has a price by_km' do
      expect(@car.price_per_km).to eq @price_km
    end
  end
end
