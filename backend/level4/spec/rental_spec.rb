# frozen_string_literal: true

require 'spec_helper'
require 'byebug'

describe '#Rental Class' do
  context 'a @rental' do
    before :each do
      @id = Faker::Number.digit
      @car = Car.new(1, 2000, 10)
      @car_id = @car.id
      @start_date = '2015-03-31'
      @end_date = '2015-04-01'
      @distance = 300
      @owner_cars = Owner.new(Faker::Name.female_first_name, [@car])
      @rental = Rental.new(@id, @car_id, @start_date, @end_date, @distance, @owner_cars)
    end

    it 'is an instance of Rental' do
      expect(@rental).to be_an_instance_of(Rental)
    end

    it 'has an id' do
      expect(@rental.id).to eq @id
    end

    it 'has start_date' do
      expect(@rental.start_date).to eq @start_date
    end

    it 'has end_date' do
      expect(@rental.end_date).to eq @end_date
    end

    it 'has distance' do
      expect(@rental.distance).to eq @distance
    end

    it 'can calculate rental days' do
      expect(@rental.rental_days).to eq 2
    end

    it 'validate rental days' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-01'
      expect { @rental.rental_days }.to raise_error(ArgumentError)
    end

    it 'validate rental days without discount by day' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-05'
      @rental.extend(PriceDiscounts)
      expect(@rental.calculate_total.to_i).to eq(5000)
    end

    it 'validate rental days without discount by  2 days' do
      expect(@rental.calculate_total.to_i).to eq 7000
    end

    it 'validate rental days with discount by  2 days' do
      @rental.extend(PriceDiscounts)
      expect(@rental.calculate_total.to_i).to eq 6800
    end

    it 'validate rental days without discount by 5 day' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-10'
      expect(@rental.calculate_total.to_i).to eq(15_000)
    end

    it 'validate rental days with discount by 5 day' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-10'
      @rental.extend(PriceDiscounts)
      expect(@rental.calculate_total.to_i).to eq(14_400)
    end

    it 'validate rental days without discount by 11 day' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-16'
      expect(@rental.calculate_total.to_i).to eq(27_000)
    end

    it 'validate rental days with discount by 11 day' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-16'
      @rental.extend(PriceDiscounts)
      expect(@rental.calculate_total.to_i).to eq(26_000)
    end

    it 'can calculate price_by_time' do
      expect(@rental.calculate_price_by_time).to eq @rental.rental_days * @car.price_per_day
    end

    it 'can calculate price_by_distance' do
      expect(@rental.calculate_price_by_distance).to eq @rental.distance * @car.price_per_km
    end

    it 'can calculate total price' do
      expect(@rental.calculate_total).to eq @rental.calculate_price_by_distance + @rental.calculate_price_by_time
    end
  end
end
