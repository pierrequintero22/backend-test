# frozen_string_literal: true

require_relative('process_data')

ProcessData.new('./data/input.json').call
