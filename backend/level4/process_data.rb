# frozen_string_literal: true

require('json')
require('date')
require_relative('owner')
require_relative('car')
require_relative('rental')

# process input file
class ProcessData
  attr_reader :cars

  def initialize(file_url)
    @file_url = file_url
    @cars = []
    @rentals = []
  end

  def call
    import_data
    set_entities
    create_cars
    create_owner_cars
    generate_rentals
    build_output_rentals
    generate_output_file
  end

  def import_data
    @data_parsed = JSON.parse(File.read(@file_url))
  rescue StandardError => e
    raise LoadError, e
  end

  def set_entities
    raise LoadError, 'file is empty' if @data_parsed.empty?

    @data_cars = @data_parsed.fetch('cars')
    @data_rentals = @data_parsed.fetch('rentals')
  end

  def create_cars
    @data_cars.each do |car|
      @cars << Car.new(car['id'],
                       car['price_per_day'],
                       car['price_per_km'])
    end
  rescue StandardError => e
    raise LoadError, e
  end

  def create_owner_cars
    @owner = Owner.new('Frank', @cars)
  end

  def generate_rentals
    @data_rentals.each do |rental|
      renta = Rental.new(rental['id'],
                         rental['car_id'],
                         rental['start_date'],
                         rental['end_date'],
                         rental['distance'], @owner)
      @rentals << renta
    end
  rescue StandardError => e
    raise LoadError, e
  end

  def build_output_rentals
    array_rentals = @rentals.each_with_object([]) do |e, array|
      e.extend(PriceDiscounts)
      array << { id: e.id, actions: build_actions(e) }
    end
    @output = { rentals: array_rentals }
  end

  def generate_output_file
    File.write('./data/output.json', JSON.dump(@output))
  end

  private

  def build_actions(rental)
    @insurance, @assistance, @divy_fee = rental.owner_cars.calculate_fees(rental.rental_days, rental.calculate_total)
    actions = []
    actions.push(driver_object(rental.calculate_total))
    actions.push(owner_object(rental.calculate_total))
    actions.push(insurance_object)
    actions.push(assistance_object)
    actions.push(divy_fees_object)
  end

  def divy_fees_object
    {
      who: 'drivy',
      type: 'credit',
      amount: @divy_fee.to_i
    }
  end

  def assistance_object
    {
      who: 'assistance',
      type: 'credit',
      amount: @assistance.to_i
    }
  end

  def insurance_object
    {
      who: 'insurance',
      type: 'credit',
      amount: @insurance.to_i
    }
  end

  def owner_object(total)
    {
      who: 'owner',
      type: 'credit',
      amount: total - (@divy_fee.to_i + @assistance.to_i + @insurance.to_i)
    }
  end

  def driver_object(total)
    {
      who: 'driver',
      type: 'debit',
      amount: total
    }
  end
end
