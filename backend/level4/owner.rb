# frozen_string_literal: true

require_relative('owner_fees')

# owner class
class Owner
  attr_reader :name, :cars

  def initialize(name, cars)
    @name = name
    @cars = cars
  end

  def add_car(car)
    @cars << car
  end

  def get_car_by_id(id)
    @cars.detect { |car| car.id == id }
  end

  def calculate_fees(days, total)
    OwnerFees.new(days, total).call
  end
end
