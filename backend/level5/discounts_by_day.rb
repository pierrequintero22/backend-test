# frozen_string_literal: true

require_relative('ranged_hash')
# discounts by day class
class DiscountsByDay
  DISCOUNTS = RangedHash.new(0..1 => 0, 2..3 => 0.1, 4..9 => 0.3, 10..nil => 0.5)

  def initialize(days, price_per_day)
    @days = days
    @price = price_per_day
  end

  def call
    @price * percentaje
  end

  def percentaje
    DISCOUNTS[@days]
  end
end
