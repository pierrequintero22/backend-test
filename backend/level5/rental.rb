# frozen_string_literal: true

require_relative('discounts_by_day')
require 'date'

# Rental class
class Rental
  attr_accessor :id, :start_date, :end_date, :distance, :owner_cars, :total, :options

  ALLOWED_OPTIONS = { gps: 500, baby_seat: 200, additional_insurance: 1000 }.freeze

  def initialize(*args)
    @id, @car_id, @start_date, @end_date, @distance, @owner_cars = args
    @options = []
    @total = ''
    retrieve_car
    calculate_total
  end

  def retrieve_car
    @car = @owner_cars.get_car_by_id(@car_id)
  end

  def calculate_total
    @total = calculate_price_by_time + calculate_price_by_distance
  end

  def rental_days
    start_date = Date.parse(@start_date)
    end_date = Date.parse(@end_date)

    raise ArgumentError, 'dates are wrong , dude!!' if start_date > end_date

    (start_date..end_date).count
  end

  def add_to_total(fee)
    @total += fee
  end

  def add_options(option)
    raise ArgumentError, 'option not permitted' unless ALLOWED_OPTIONS.keys.include?(option.to_sym)

    @options << option
  end

  def calculate_price_by_time
    rental_days * @car.price_per_day
  end

  def calculate_price_by_distance
    @distance * @car.price_per_km
  end
end

# module to price discounts
module PriceDiscounts
  def calculate_total
    super - DiscountsByDay.new(rental_days, @car.price_per_day).call
  end
end
