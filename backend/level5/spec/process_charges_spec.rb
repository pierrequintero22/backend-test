# frozen_string_literal: true

require 'spec_helper'

describe '#processing charges' do
  context 'a @processing charges' do
    before :each do
      @id = Faker::Number.digit
      @car = Car.new(1, 2000, 10)
      @car_id = @car.id
      @start_date = '2015-03-31'
      @end_date = '2015-04-01'
      @distance = 300
      @owner_cars = Owner.new(Faker::Name.female_first_name, [@car])
      @rental = Rental.new(@id, @car_id, @start_date, @end_date, @distance, @owner_cars)
      @driver= Driver.new
      @drivy= Drivy.new
      @insurance= Insurance.new
      @assistance= Assistance.new
      @rental.extend(PriceDiscounts)
      ProcessCharges.new(@rental, @driver, @drivy, @insurance, @assistance).call
      @rental_fees= @rental.calculate_total * 0.3 # 30% from rental price
    end

    it 'add charges to driver' do
      expect(@driver.charges).to eq(@rental.calculate_total)
    end

    it 'add charges to insurance' do
      expect(@insurance.charges).to eq(@rental_fees / 2)
    end

    it 'add charges to assistance' do
      expect(@assistance.charges).to eq(@rental.rental_days * 100)
    end

    it 'add charges to assistance' do
      expect(@drivy.charges).to eq(@rental_fees - (@rental_fees / 2 + @rental.rental_days * 100))
    end

    it 'add charges to assistance' do
      expect(@owner_cars.charges).to eq(@driver.charges-(@drivy.charges+@rental_fees / 2 + @rental.rental_days * 100))
    end
  end
end
