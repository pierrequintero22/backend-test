# frozen_string_literal: true

require 'spec_helper'
require 'byebug'

describe '#Process Data Class' do
  context 'a Data' do
    before :each do
      file_url = './data/input.json'
      @process_data = ProcessData.new(file_url)
    end

    it 'is an instance of ProcessData' do
      expect(@process_data).to be_an_instance_of(ProcessData)
    end
  end
end
