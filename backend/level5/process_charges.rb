# frozen_string_literal: true

# process all charges
class ProcessCharges
  OPTIONS_FEES = { gps: 'owner', baby_seat: 'owner', additional_insurance: 'drivy' }.freeze

  def initialize(*args)
    @rental, @driver, @drivy, @insurance, @assistance = args
  end

  def call
    set_variables
    reset_charges
    calculate_fees
    calculate_insurance
    calculate_assistance
    calculate_divy_fee
    calculate_driver_fee
    calculate_owner_fee
    calculate_options
  end

  def set_variables
    @owner = @rental.owner_cars
    @options = @rental.options
    @total = @rental.calculate_total
    @days = @rental.rental_days
  end

  def reset_charges
    @owner.reset_charges
    @driver.reset_charges
    @insurance.reset_charges
    @assistance.reset_charges
    @drivy.reset_charges
  end

  def calculate_fees
    @fee = @total.to_i * 0.3
  end

  def calculate_insurance
    @insurance_fee = @fee / 2
    @insurance.add_charge(@insurance_fee)
  end

  def calculate_assistance
    @assistance_fee = @days * 100
    @assistance.add_charge(@assistance_fee)
  end

  def calculate_divy_fee
    @drivy_fee = @fee - @insurance_fee - @assistance_fee
    @drivy.add_charge(@drivy_fee)
  end

  def calculate_driver_fee
    @driver.add_charge(@rental.calculate_total)
  end

  def calculate_owner_fee
    @owner_fee = @total - (@drivy_fee + @assistance_fee + @insurance_fee)
    @owner.add_charge(@owner_fee)
  end

  def calculate_options
    return if @options.empty?

    @options.each_with_object([]) do |option, _array|
      fee = Rental::ALLOWED_OPTIONS[option.to_sym]
      total_fee = fee * @days
      charge_fee_to_entity(option, total_fee)
    end
  end

  private

  def charge_fee_to_entity(option, total_fee)
    entity = OPTIONS_FEES[option.to_sym]
    fee_entity = instance_variable_get("@#{entity}")
    fee_entity.add_charge(total_fee)
    @driver.add_charge(total_fee)
  end
end
