# frozen_string_literal: true

# entities base class
class Entity
  def initialize
    @charges = []
  end

  def charges
    @charges.sum
  end

  def add_charge(charge)
    @charges << charge.to_i
  end

  def default_action
    'credit'
  end

  def reset_charges
    @charges = []
  end
end
