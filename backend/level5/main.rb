# frozen_string_literal: true

require_relative('process_data')

ProcessData.new('./data/input.json').call

# used gems
# rspec
# byebug
# faker
# rubocop
