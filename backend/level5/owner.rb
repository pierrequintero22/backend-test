# frozen_string_literal: true

require_relative('entity')
# owner class
class Owner < Entity
  attr_reader :name, :cars

  def initialize(name, cars)
    super()
    @name = name
    @cars = cars
  end

  def add_car(car)
    @cars << car
  end

  def get_car_by_id(id)
    @cars.detect { |car| car.id == id }
  end
end
