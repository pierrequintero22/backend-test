# frozen_string_literal: true

# Driver class
class Driver < Entity
  def default_action
    'debit'
  end
end
