# frozen_string_literal: true

require('json')
require('date')
require 'active_support/all'
require_relative('owner')
require_relative('car')
require_relative('rental')
require_relative('driver')
require_relative('drivy')
require_relative('insurance')
require_relative('assistance')
require_relative('process_charges')

# process input file
class ProcessData
  attr_reader :cars

  ACTIONS_ENTITIES = %w[driver owner insurance assistance drivy].freeze

  def initialize(file_url)
    @file_url = file_url
    @cars = []
    @rentals = []
  end

  def call
    import_data
    set_entities
    create_cars
    create_owner_cars
    generate_rentals
    add_options
    build_output_rentals
    generate_output_file
  end

  def import_data
    @data_parsed = JSON.parse(File.read(@file_url))
  rescue StandardError => e
    raise LoadError, e
  end

  def set_entities
    raise LoadError, 'file is empty' if @data_parsed.empty?

    @driver = Driver.new
    @insurance = Insurance.new
    @assistance = Assistance.new
    @drivy = Drivy.new
    @data_cars = @data_parsed.fetch('cars')
    @data_rentals = @data_parsed.fetch('rentals')
    @options = @data_parsed.fetch('options')
  end

  def create_cars
    @data_cars.each do |car|
      @cars << Car.new(car['id'],
                       car['price_per_day'],
                       car['price_per_km'])
    end
  rescue StandardError => e
    raise LoadError, e
  end

  def create_owner_cars
    @owner = Owner.new('Frank', @cars)
  end

  def generate_rentals
    @data_rentals.each do |rental|
      renta = Rental.new(rental['id'],
                         rental['car_id'],
                         rental['start_date'],
                         rental['end_date'],
                         rental['distance'], @owner)
      @rentals << renta
    end
  rescue StandardError => e
    raise LoadError, e
  end

  def add_options
    return if @options.empty?

    @options.each do |option|
      rental = @rentals.detect { |r| r.id == option['rental_id'] }
      rental.add_options(option['type'])
    end
  end

  def build_output_rentals
    array_rentals = @rentals.each_with_object([]) do |e, array|
      e.extend(PriceDiscounts)
      ProcessCharges.new(e, @driver, @drivy, @insurance, @assistance).call
      array << { id: e.id, options: e.options, actions: generate_actions }
    end
    @output = { rentals: array_rentals }
  end

  def generate_output_file
    File.write('./data/output.json', JSON.dump(@output))
  end

  private

  def generate_actions
    ACTIONS_ENTITIES.each_with_object([]) do |action, array|
      array.push(build_action(action))
    end
  end

  def build_action(action)
    action_instance = instance_variable_get("@#{action}")
    {
      who: action,
      type: action_instance.default_action,
      amount: action_instance.charges
    }
  end
end
