require_relative('ranged_hash')

class DiscountsByDay
  DISCOUNTS= RangedHash.new(0..1=> 0, 2..3=> 0.1, 4..9=> 0.3, 10..=> 0.5 )

  def initialize(days, price_per_day)
    @days= days
    @price = price_per_day
  end

  def call
    @price * percentaje
  end

  def percentaje
    DISCOUNTS[@days]
  end
end