# frozen_string_literal: true

# owner_fees
class OwnerFees
  def initialize(days, total)
    @days = days
    @total = total
  end

  def call
    calculate_total_fee
    calculate_insurance
    calculate_assistance
    calculate_divy_fee
    [@insurance, @assistance, @divy_fee]
  end

  def calculate_total_fee
    @fee = @total * 0.3
  end

  def calculate_insurance
    @insurance = @fee / 2
  end

  def calculate_assistance
    @assistance = @days * 100
  end

  def calculate_divy_fee
    @divy_fee = @fee - @insurance - @assistance
  end
end
