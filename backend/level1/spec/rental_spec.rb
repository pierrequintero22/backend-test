# frozen_string_literal: true

require 'spec_helper'

describe '#Rental Class' do
  context 'a @rental' do
    before :each do
      @id = Faker::Number.digit
      @car = Car.new(1, 2, 3)
      @car_id = @car.id
      @start_date = '2021-12-01'
      @end_date = '2021-12-03'
      @distance = Faker::Number.number(digits: 3)
      @owner_cars = Owner.new(Faker::Name.female_first_name, [@car])
      @rental = Rental.new(@id, @car_id, @start_date, @end_date, @distance, @owner_cars)
    end

    it 'is an instance of Rental' do
      expect(@rental).to be_an_instance_of(Rental)
    end

    it 'has an id' do
      expect(@rental.id).to eq @id
    end

    it 'has start_date' do
      expect(@rental.start_date).to eq @start_date
    end

    it 'has end_date' do
      expect(@rental.end_date).to eq @end_date
    end

    it 'has distance' do
      expect(@rental.distance).to eq @distance
    end

    
    it 'can calculate price_by_time' do
      expect(@rental.calculate_price_by_time).to eq @rental.rental_days * @car.price_per_day
    end
    
    it 'can calculate price_by_distance' do
      expect(@rental.calculate_price_by_distance).to eq @rental.distance * @car.price_per_km
    end
    
    it 'can calculate rental days' do
      expect(@rental.rental_days).to eq 3
    end

    it 'validate rental days' do
      @rental.start_date = '2021-12-05'
      @rental.end_date = '2021-12-01'
      expect { @rental.rental_days }.to raise_error(ArgumentError)
    end

    it 'can calculate total price' do
      expect(@rental.calculate_total).to eq @rental.calculate_price_by_distance + @rental.calculate_price_by_time
    end
  end
end
