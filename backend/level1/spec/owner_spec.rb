# frozen_string_literal: true

require 'spec_helper'

describe '#Owner' do
  context 'Owner' do
    before :each do
      @cars = [Car.new(1, 2, 3), Car.new(2, 3, 4)]
      @name = Faker::Name.female_first_name
      @owner = Owner.new(@name, @cars)
    end

    it 'is an instance of Owner' do
      expect(@owner).to be_an_instance_of(Owner)
    end

    it 'has a name' do
      expect(@owner.name).to eq(@name)
    end

    it 'has cars' do
      expect(@owner.cars).to eq(@cars)
    end

    it 'can add a new car' do
      @owner.add_car(Car.new(7, 7, 7))
      expect(@owner.cars.size).to eq 3
    end

    it 'can list cars' do
      expect(@owner.cars).to be_an_instance_of(Array)
        .and eq(@cars)
    end

    it 'can get a car by id' do
      expect(@owner.get_car_by_id(1)).to be_truthy
    end
  end
end
