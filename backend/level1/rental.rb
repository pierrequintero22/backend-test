# frozen_string_literal: true

# Rental class
class Rental
  attr_accessor :id, :start_date, :end_date, :distance

  def initialize(*args)
    @id, @car_id, @start_date, @end_date, @distance, @owner_cars = args
    retrieve_car
  end

  def retrieve_car
    @car = @owner_cars.get_car_by_id(@car_id)
  end

  def rental_days
    start_date = Date.parse(@start_date)
    end_date = Date.parse(@end_date)

    raise ArgumentError, 'dates are wrong , dude!!' if start_date > end_date

    (start_date..end_date).count
  end

  def calculate_price_by_time
    rental_days * @car.price_per_day
  end

  def calculate_price_by_distance
    @distance * @car.price_per_km
  end

  def calculate_total
    calculate_price_by_time + calculate_price_by_distance
  end
end
