# frozen_string_literal: true

require_relative '../car'
require_relative '../owner'
require_relative '../rental'
require_relative '../process_data'

require 'faker'
require 'byebug'
